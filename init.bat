echo off
cd %userprofile%
set STIME=%TIME::=%
set now=%DATE:~-4%%DATE:~-10,2%%DATE:~-7,2%%STIME:~6%
echo %now%
echo %STIME%

:: .bashrc

dir /A:L .bash_profile 2>&1 | find "SYMLINK" > null  && (
	echo "Link .profile exists."
) || ( 
	echo "Linking .profile"
	if exist .bash_profile ( move .bash_profile .dotfiles\backups\.profile-%now% )
	mklink .bash_profile .dotfiles\bash\profile
) 

dir /A:L .bashrc 2>&1 | find "SYMLINK" > null  && (
	echo "Link .bashrc exists."
) || ( 
	echo "Linking .bashrc."
	if exist .bashrc ( move .bashrc .dotfiles\backups\.bashrc-%now% )
	mklink .bashrc .dotfiles\bash\rc
) 

:: .dircolors

dir /A:L .dircolors 2>&1 | find "SYMLINK" > null && (
	echo "Link .dircolors exists."
) || ( 
	echo "Linking .dircolors."
	if exist .dircolors ( move .dircolors .dotfiles\backups\.dircolors-%now% )
	mklink .dircolors .dotfiles\bash\dircolors
)

:: .inputrc

dir /A:L .inputrc 2>&1 | find "SYMLINK" > null && (
	echo "Link .inputrc exists."
) || ( 
	echo "Linking .inputrc."
	if exist .inputrc ( move .inputrc .dotfiles\backups\.inputrc-%now% )
	mklink .inputrc .dotfiles\bash\inputrc
)

:: .vimrc

dir /A:L .vimrc 2>&1 | find "SYMLINK" > null && (
	echo "Link .vimrc exists"
) || ( 
	echo "Linking .vimrc."
	if exist .vimrc ( move .vimrc .dotfiles\backups\.vimrc-%now% )
	mklink .vimrc .dotfiles\vimrc
)

:: .vim

:: if [ -d .vim ]; then
	:: mv .vim .dotfiles/backups/.vim-$(date -d "today" +"%Y%m%d%H%M")
:: fi
:: if [ ! -L .vim ]; then
	:: ln -s .dotfiles/vim .vim
:: fi

:: .ssh

dir /A:L .ssh/config 2>&1 | find "SYMLINK" > null && (
	echo "Link .ssh/config exists"
) || ( 
	echo "Linking .ssh/config"
	cd .ssh
	if exist config ( move config ..\.dotfiles\backups\config-%now% )
	mklink config ..\.dotfiles\ssh\config
	cd ..
)

:: git

dir /A:L .gitconfig 2>&1 | find "SYMLINK" > null && ( 
	echo "Link .gitconfig exists"
) || ( 
	if exist .gitconfig ( move .gitconfig .dotfiles\backups\.gitconfig-%now% )
	mklink .gitconfig .dotfiles\gitconfig
)

:: tmux

dir /A:L .tmux.conf 2>&1 | find "SYMLINK" > null && (
	echo "Link .tmux.conf exists"
) || ( 
	echo "Linking .tmux.conf"
	if exist .tmux.conf ( move .tmux.conf .dotfiles\backups\.tmux-%now% )
	mklink .tmux.conf .dotfiles\tmux
)
