os=`uname`
env=${os:0:7}
cd ~

### .profile

if [ -f .profile ]; then
	mv .profile .dotfiles/backups/.profile-$(date -d "today" +"%Y%m%d%H%M")
fi
if [ ! -L .profile ]; then
	ln -s .dotfiles/bash/profile .profile
fi

### .bashrc

if [ -f .bashrc ]; then
	mv .bashrc .dotfiles/backups/.bashrc-$(date -d "today" +"%Y%m%d%H%M")
fi
if [ ! -L .bashrc ]; then
	ln -s .dotfiles/bash/rc .bashrc
fi

### .dircolors

if [ -f .dircolors ]; then
	mv .dircolors .dotfiles/backups/.dircolors-$(date -d "today" +"%Y%m%d%H%M")
fi
if [ ! -L .dircolors ]; then
	ln -s .dotfiles/bash/dircolors .dircolors
fi

### .inputrc

if [ -f .inputrc ]; then
	mv .inputrc .dotfiles/backups/.inputrc-$(date -d "today" +"%Y%m%d%H%M")
fi
if [ ! -L .inputrc ]; then
	ln -s .dotfiles/bash/inputrc .inputrc
fi

### .vimrc

if [ -f .vimrc ]; then
	mv .vimrc .dotfiles/backups/.vimrc-$(date -d "today" +"%Y%m%d%H%M")
fi
if [ ! -L .vimrc ]; then
	ln -s .dotfiles/vimrc .vimrc
fi

### .vim

if [ -d .vim ]; then
	mv .vim .dotfiles/backups/.vim-$(date -d "today" +"%Y%m%d%H%M")
fi
if [ ! -L .vim ]; then
	ln -s .dotfiles/vim .vim
fi

if [ "$env" = "MINGW64" ] && [ ! -L .vim/colors ]; then
	cd .vim
	ln -s _colors/16 colors
	cd ..
elif [ ! -L .vim/colors ]; then
	cd .vim
	ln -s _colors/256 colors
	cd ..
fi

### .ssh

if [ ! -d .ssh ]; then
	mkdir .ssh
	chmod 700 .ssh
fi
if [ -f .ssh/config ]; then
	mv .ssh/config .dotfiles/backups/config-$(date -d today +"%Y%m%d%H%M")
fi
if [ ! -L .ssh/config ]; then
	cd .ssh
	ln -s ~/.dotfiles/ssh/config config
	cd ..
fi

### git

if [ -f .gitconfig ]; then
	mv .gitconfig .dotfiles/backups/.gitconfig-$(date -d "today" +"%Y%m%d%H%M")
fi
if [ ! -L .gitconfig ]; then
	ln -s .dotfiles/gitconfig .gitconfig
fi

### tmux

if [ -f .tmux.conf ]; then
	mv .tmux.conf .dotfiles/backups/.tmux.conf-$(date -d "today" +"%Y%m%d%H%M")
fi
if [ ! -L .tmux.conf ]; then
	ln -s .dotfiles/tmux .tmux.conf
fi
