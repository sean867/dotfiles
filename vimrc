let os = system("uname")
let env = os[0:6]

if env != 'MINGW64' && env != 'MSYS_NT'
	" Local dirs
	set backupdir=~/.vim/backups
	set directory=~/.vim/swaps
	set undodir=~/.vim/undo
	let g:colorsbox_contrast_dark='hard'
	color colorsbox-stnight
endif

" Make vim more useful
set nocompatible

" Set syntax highlighting options.
set background=dark
set tabstop=4 shiftwidth=4
set t_ut=

" Other options
set autoindent " Copy indent from last line when starting new line.
set backspace=indent,eol,start
" set cursorline " Highlight current line
set diffopt=filler " Add vertical spaces to keep right and left aligned
set diffopt+=iwhite " Ignore whitespace changes (focus on code changes)
set encoding=utf-8 nobomb " BOM often causes trouble

" make searches case insensitive by default, but switch to case sensitive
" if you use case in your search term
set ignorecase smartcase

set term=xterm-256color
syntax on
filetype plugin indent on
